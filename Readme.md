# README

Offering is an app that allow you to make beautiful PDF using Latex and Markdown.

You also can use it in order to produce docx


# INSTALL

* install texlive suite and pandoc

```
#!bash
sudo su
apt-get install texlive
apt-get install texlive-basic
apt-get install pandoc
```

* Make the differents directories
```
#!bash
mkdir NotesFolder
cd NotesFolder
git clone git@bitbucket.org:leotrouvtou/offering.git
mkdir motions
```
You tree should look like that

```
#!bash
.
├── motions
└── offering
    ├── Readme.md
    ├── shared
    │   ├── default.latex
    │   ├── init.sh
    │   └── default.log
    └── template
        ├── footer.tex
        ├── logo.jpg
        └── Makefile
```

Make 

`cp offering/shared/init.sh .` 

and

`chmod +x init.sh`

# Configuration

Edit the file footer.tex to have your footer in the file. you can find a documentation of latex on http://latex-project.org/guides/

Change logo.jpg to have your own

# Use 

When you want to do a new folder, simply do :


```
#!bash

leotrouvtou@mylaptop:~/Documents/NotesFolder$ ./init.sh 
What is the name of your folder ? (enter the name) : newFolder
```


# Compilation

go to the folder of you choice an simply do 

```
#!bash

make pdfs
```

 or

```
#!bash

make docx
```



Thanx to Jérôme : [https://github.com/jeromenerf](https://github.com/jeromenerf) for the concept and the technical advices